package com.bunhann.fragmenttypes;

public interface UpdateListener {

    public void uncheckedUpdate(boolean update);

}
