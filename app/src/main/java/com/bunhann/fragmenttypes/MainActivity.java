package com.bunhann.fragmenttypes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, UpdateListener{

    private Button btnFirstFragment, btnSecondFragment;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnFirstFragment = (Button) findViewById(R.id.btnFirstFragment);
        btnSecondFragment = (Button) findViewById(R.id.btnSecondFragment);

        tvName = (TextView) findViewById(R.id.txtFromFragment);

        btnFirstFragment.setOnClickListener(this);
        btnSecondFragment.setOnClickListener(this);

    }

    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFirstFragment:
                loadFragment(FirstFragment.newInstance(4, "Bunhann"));
                return;
            case R.id.btnSecondFragment:
                loadFragment(new SecondFragment());
                return;
            default:
                return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
            case R.id.toast:
                new FirstFragment().helloFromFragement(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void setName(String name){
        tvName.setText("Text from First Fragement: " + name.toUpperCase());
    }

    @Override
    public void uncheckedUpdate(boolean update) {
        if (update){
            Toast.makeText(this, "You click on TextView in Second Fragment", Toast.LENGTH_SHORT).show();
            tvName.setText("Text from Second Fragement: " + String.valueOf(update));
        }
    }
}
