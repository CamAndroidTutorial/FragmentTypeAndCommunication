package com.bunhann.fragmenttypes;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FirstFragment extends Fragment {

    private Button firstButton;
    private TextView tvInt, tvString;
    private EditText edName;

    public static FirstFragment newInstance(int myInt, String myString){
        FirstFragment firstFragment = new FirstFragment();
        Bundle args = new Bundle();
        args.putInt("MYINT", myInt);
        args.putString("MYSTRING", myString);
        firstFragment.setArguments(args);
        return firstFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        printLog("onActivityCreated Called");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.first_fragment, container, false);

        firstButton = (Button) view.findViewById(R.id.firstButton);
        tvInt = (TextView) view.findViewById(R.id.txtFromActivity1Int);
        tvString = (TextView) view.findViewById(R.id.txtFromActivity1String);
        edName = (EditText) view.findViewById(R.id.edName);

        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edName.getText().length()>0){
                    ((MainActivity) getActivity()).setName(edName.getText().toString());
                } else
                {
                    Snackbar snackbar = Snackbar
                            .make(v, "Button Pressed on First Fragment", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        printLog("onCreateView Called");
        int someInt = getArguments().getInt("MYINT", 0);
        String someString = getArguments().getString("MYSTRING", "EMPTY");
        tvInt.append(String.valueOf(someInt));
        tvString.append(someString);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        printLog("onViewCreated Called");

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        printLog("onAttach Activity Called");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        printLog("onCreate Called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        printLog("onDestroy Called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        printLog("onDestroyView Called");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        printLog("onDetach Called");
    }

    @Override
    public void onPause() {
        super.onPause();
        printLog("onPause Called");
    }

    @Override
    public void onResume() {
        super.onResume();
        printLog("onResume Called");
    }

    @Override
    public void onStart() {
        super.onStart();
        printLog("onStart Called");
    }

    @Override
    public void onStop() {
        super.onStop();
        printLog("onStop Called");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        printLog("onAttach context Called");
    }

    private void printLog(String s) {
        // display a message in Log File
        Log.d("LifeCycle:", s);
    }

    protected void helloFromFragement(Context context){
        Toast.makeText(context, "I'm a toast from First Fragment!", Toast.LENGTH_SHORT).show();
    }
}
