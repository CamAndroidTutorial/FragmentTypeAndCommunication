package com.bunhann.fragmenttypes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

public class MyDialogFragment extends DialogFragment {

    private Button btnDismiss;

    public static MyDialogFragment newInstance() {
        MyDialogFragment frag = new MyDialogFragment();
//        Bundle args = new Bundle();
//        args.putString(TITLE, dataToShow);
//        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment, null);
        setCancelable(false);
        builder.setView(view);
        final Dialog dialog = builder.create();

        btnDismiss = (Button) view.findViewById(R.id.btnDismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }
}

